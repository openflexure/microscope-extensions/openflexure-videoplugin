# OFM Video Extension

A video recording extension for the OpenFlexure Microscope.

## Installation

To use, download the extension and save it in `/var/openflexure/extensions/microscope_extensions` on your microscope. (You may need to [set your user permissions](https://openflexure.gitlab.io/microscope-handbook/develop-extensions/index.html#set-permissions).)

You will also need to install [`ffmpeg-python`](https://github.com/kkroening/ffmpeg-python):

`ofm activate`  
`pip install ffmpeg-python`

For more information about extensions, see the [API documentation](https://openflexure-microscope-software.readthedocs.io/en/master/plugins.html) and the [handbook](https://openflexure.gitlab.io/microscope-handbook/develop-extensions/index.html).

## Notes on usage

Recorded videos will not appear in the gallery.  They will be stored in the current `micrographs/Videos` folder (default is `/var/openflexure-microscope/data/micrographs/Videos`).  You can use [`scp`](https://www.raspberrypi.org/documentation/computers/remote-access.html#using-secure-copy) or [`winscp`](https://winscp.net/eng/index.php) to transfer the videos to another computer.
