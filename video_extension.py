import logging
from contextlib import nullcontext
from pathlib import Path

import ffmpeg
from flask import abort, send_file
from labthings import fields, find_component
from labthings.extensions import BaseExtension
from labthings.views import ActionView
from openflexure_microscope.api.utilities.gui import build_gui
from openflexure_microscope.captures.capture_manager import generate_basename

## Extension methods
def record_video(
    microscope,
    filename,
    folder,
    video_length,
    video_framerate,
    lock_stage,
    video_format: str = "h264",
    metadata: dict = {},
    annotations: dict = {},
    tags: list = [],
):
    # Create video output
    output = microscope.captures.new_video(
        temporary=False, filename=filename, folder=folder, fmt="h264"
    )

    # Do recording
    if lock_stage:
        cm = microscope.stage.lock(), microscope.camera.lock
    else:
        cm = nullcontext(), microscope.camera.lock

    with cm[1]:
        # Set minimum resolution if framerate is higher than 30fps, maximum of 90 fps in this configuration
        # To Do: add different modes depending on resolution/framerate combinations
        old_stream_resolution = microscope.camera.stream_resolution
        if video_framerate > 30:
            microscope.camera.stream_resolution = (640,480)

        # Stop the stream during acquisition. Important for high framerates due to bandwith limitations
        microscope.camera.stop_stream()
        microscope.camera.picamera.framerate = video_framerate

        microscope.camera.start_recording(output=output.file, fmt="h264")
        microscope.camera.picamera.wait_recording(timeout=video_length, splitter_port=2)
        microscope.camera.stop_recording()
        microscope.camera.stream_resolution = old_stream_resolution
        microscope.camera.start_stream()

    output_file = output.file
    # Convert the h264 stream to a different file format on user choice
    if video_format != "h264": 
        input_file = output.file
        output_file = input_file[:-4] + video_format
        (ffmpeg.input(input_file, r=video_framerate).output(output_file, r=video_framerate).run())
    return output_file

class GetVideoRecording(ActionView):
    """
    Get a video from file location
    """

    args = {
        "video_file_location": fields.String(
            description="The video file location returned by record_video"
        )
    }

    def post(self, args):
        video_file_location = args.get("video_file_location")
        file_path = Path(video_file_location)
        if not file_path.exists():
            abort(404, description="Video file not found.")
        else:
            logging.info(f"Getting video from {file_path}")
            return send_file(file_path)


## Extension views
class VideoAPI(ActionView):
    """
    Record a video
    """

    args = {
        "video_length": fields.Number(
            missing=1, example=1, description="Length (seconds) of video"
        ),
        "video_framerate": fields.Number(
            missing=30, example=30, description="Video framerate (frames per second)"
        ),
        "video_format": fields.String(
            missing="h264", example="h264", description="Video format (h264,...)"
        ),
        "lock_stage": fields.List(
            fields.String,
            missing=[],
            allow_none=True,
            description="Is the stage locked during recording?",
        ),
        "filename": fields.String(
            missing = generate_basename(),
            example = generate_basename(),
            allow_none = True,
            description = "Name of the video file"
        ),
    }

    def post(self, args):

        video_length = args.get("video_length")
        video_format = args.get("video_format")
        video_framerate = args.get("video_framerate")
        lock_stage = args.get("lock_stage")
        filename = args.get("filename")
        logging.info("Filename: {}".format(filename))

        # Find our microscope component
        microscope = find_component("org.openflexure.microscope")

        # Location to store video
        folder = "Videos"
        if not filename:
            logging.info("No name given")
            basename = generate_basename()
            filename = f"{basename}"
            logging.info(filename)

        logging.info("Filename: {}".format(filename))
        return record_video(
            microscope,
            filename=filename,
            folder=folder,
            video_length=video_length,
            video_format=video_format,
            video_framerate=video_framerate,
            lock_stage=True if lock_stage == "Locked" else False,
            metadata=microscope.metadata,
        )


## Extension GUI (OpenFlexure eV)
# Alternate form without any dynamic parts
extension_gui = {
    "icon": "videocam",  # Name of an icon from https://material.io/resources/icons/
    "forms": [  # List of forms. Each form is a collapsible accordion panel
        {
            "name": "Record a video",  # Form title
            "route": "/video",  # The URL rule (as given by "add_view") of your submission view
            "isTask": True,  # This forms submission starts a background task
            "isCollapsible": False,  # This form cannot be collapsed into an accordion
            "submitLabel": "Start recording",  # Label for the form submit button
            "schema": [  # List of dictionaries. Each element is a form component.
                {
                    "fieldType": "numberInput",
                    "name": "video_length",
                    "label": "Length (seconds) of video",
                    "min": 0.1,  # HTML number input attribute
                    "step": 0.1,  # HTML number input attribute
                    "default": 5,  # HTML number input attribute
                },
                {
                    "fieldType": "numberInput",
                    "name": "video_framerate",
                    "label": "Framerate (Frames Per Second)",
                    "min": 1,  # HTML number input attribute
                    "max": 90,
                    "step": 1,  # HTML number input attribute
                    "default": 30,  # HTML number input attribute
                },
                {
                    "fieldType": "selectList",
                    "name": "video_format",
                    "label": "Video format",
                    "value": "h264",
                    "options": [
                        "h264",
                        "mjpeg",
                        "yuv",
                        "rgb",
                        "rgba",
                        "bgr",
                        "bgra",
                        "mp4",
                    ],
                },
                {
                    "fieldType": "checkList",
                    "name": "lock_stage",
                    "label": "Lock stage",
                    "value": [],
                    "options": ["Locked"],
                },
                {
                    "fieldType": "textInput",
                    "name": "filename",
                    "label": "Video filename",
                    "value": generate_basename(),
                    "default": generate_basename(),
                    "placeholder": generate_basename(),
                },
            ],
        }
    ],
}


## Create extension

# Create your extension object
video_extension = BaseExtension("org.openflexure.video_extension", version="0.0.0")

# Add methods to your extension
video_extension.add_method(record_video, "record_video")

# Add API views to your extension
video_extension.add_view(VideoAPI, "/video")
video_extension.add_view(GetVideoRecording, "/get-video")

# Add OpenFlexure eV GUI to your extension
video_extension.add_meta("gui", build_gui(extension_gui, video_extension))
